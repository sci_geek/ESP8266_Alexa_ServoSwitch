#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <functional>
#include "switch.h"
#include "UpnpBroadcastResponder.h"
#include "CallbackFunction.h"
#include "LightSwitch.h"

//servo pins
#define SERVO1_PIN 4
#define SERVO2_PIN 5

//servo Calubration (ON must me higher then OFF)
#define ON_POSITION 110
#define NUTRAL_POSITION 65
#define OFF_POSITION 15

// prototypes
boolean connectWifi();

//on/off callbacks
void Switch1_On();
void Switch1_Off();
void Switch2_On();
void Switch2_Off();

// Change this before you flh
const char* ssid = "Your wifi";
const char* password = "your password";

boolean wifiConnected = false;

UpnpBroadcastResponder upnpBroadcastResponder;

Switch *switch1 = NULL;
Switch *switch2 = NULL;

LightSwitch *ServoSwitch1 = NULL;
LightSwitch *ServoSwitch2 = NULL;

void setup()
{
  Serial.begin(9600);

  // Initialise wifi connection
  wifiConnected = connectWifi();

  if(wifiConnected){
    upnpBroadcastResponder.beginUdpMulticast();

    // Define your switches here. Max 14
    // Format: Alexa invocation name, local port no, on callback, off callback
    switch1 = new Switch("fan", 80, Switch1_On, Switch1_Off);
    switch2 = new Switch("lights", 81, Switch2_On, Switch2_Off);

    //livingroom fan
    ServoSwitch1 = new LightSwitch(SERVO1_PIN, ON_POSITION, OFF_POSITION, NUTRAL_POSITION);
    //livingroom lights

    Serial.println("Adding switches upnp broadcast responder");
    upnpBroadcastResponder.addDevice(*switch1);
    upnpBroadcastResponder.addDevice(*switch2);
  }
}

void loop()
{
  if(wifiConnected){
    upnpBroadcastResponder.serverLoop();

    switch1->serverLoop();
    switch2->serverLoop();
  }
}

void Switch1_On() {
  Serial.print("Switch 1 turn on ...");
  ServoSwitch1->moveOn();
}

void Switch1_Off() {
  Serial.print("Switch 1 turn off ...");
  ServoSwitch1->moveOff();
}

void Switch2_On() {
  Serial.print("Switch 2 turn on ...");
  ServoSwitch2->moveOn();
}

void Switch2_Off() {
  Serial.print("Switch 2 turn off ...");
  ServoSwitch2->moveOff();
}

// connect to wifi – returns true if successful or false if not
boolean connectWifi(){
  boolean state = true;
  int i = 0;

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting ...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 10){
      state = false;
      break;
    }
    i++;
  }

  if (state){
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("");
    Serial.println("Connection failed.");
  }

  return state;
}
